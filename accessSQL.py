import base_logger as bl
from helperFuncs import callProcess

class SQL_database: 
    """
    a class responsible for the interaction with the sqlite database
    """

    def __init__(self, db_path): 
        self.db_path = db_path

        #if db file doesnt exist create one
        (output, pstatus, err) = callProcess("test -e {} && echo '1' || echo '0'".format(db_path))
        if output.decode() != "1\n":
            bl.logger.warning("database file not found at {}, creating new file".format(db_path))
            (output, pstatus, err) = callProcess("sqlite3 {} ';'".format(db_path))
        

    def createTables(self):
        """
        creates an events table where the summerized logs will be stored
        """
        cmdstr="""sqlite3 {} 'CREATE TABLE IF NOT EXISTS events (
            event_id INT PRIMARY_KEY UNIQUE NOT NULL,
            date TEXT NOT NULL,
            time TEXT NOT NULL, 
            success TEXT NOT NULL,
            user_id INT NOT NULL,
            username TEXT NOT NULL,
            desc TEXT NOT NULL
        );'
        """.format(self.db_path)
        (output, pstatus, err) = callProcess(cmdstr)
        bl.logger.info("basic events table created successfuly")

    def insertEvent(self, event):
        """
        this function takes an event obj and adds it to the SQL table

        args:
            event: the event obj to be stored in the table
        """
        cmdstr="""sqlite3 {} 'INSERT INTO events VALUES ({}, "{}", "{}", "{}", {}, "{}", "{}")'""".format(self.db_path, event.id, event.date[0], event.time, event.success[0], event.user_id, event.username, event.desc)
        (output, pstatus, err) = callProcess(cmdstr)

    def getEvents(self):
        """
        this function prints the events from the database
        """
        cmdstr="""sqlite3 {} 'SELECT * FROM events'""".format(self.db_path)
        (output, pstatus, err) = callProcess(cmdstr)
        return output.decode()

    def clearDatabase(self):
        """
        clear database enteries before run
        """
        bl.logger.info("deleting all enteries before start")
        cmdstr = "sqlite3 {} 'DELETE FROM events'".format(self.db_path)
        (output, pstatus, err) = callProcess(cmdstr)