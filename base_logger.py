"""
    Logging module for global use.
"""
import logging
import logging.config


def loggerFile(file_name):
    """
    Load a logger configuration file into logging.

    Args:
        file_name (str): logger config file
    """
    logging.config.fileConfig(file_name)


def loggerNameInit(logger_name):
    """
    Initialize logger with given logger name.

    Args:
        logger_name (str): logger name
    """
    # To change global logger
    global logger
    # This logger now has StreamHandler (DEBUG Level) and FileHandler (INFO Level) and the specified format
    logger = logging.getLogger(logger_name)
