from helperFuncs import callProcess
from eventObj import Event
import base_logger as bl
import sys
import regex

class Audit:
    """
    a calss responsible for handling the audit logs
    """

    def __init__(self, path):
        """
        preperes the variables to be used in the functions. runs when the obj is assembled 
        
        args:
            path: a path to the directory in which the log files are kept
        """
        self.log_files_path = path 
        cmdstr="""ls {}""".format(self.log_files_path) #get list of log files
        (output, pstatus, err) = callProcess(cmdstr)
        self.log_files_list = output.decode().split("\n") 
        self.log_files_list.pop()
        if self.log_files_list == []:
            bl.logger.error("no auditd log files found")
            sys.exit(1)
        else:
            for file_name in self.log_files_list:
                if "audit.log" not in file_name:
                    bl.logger.warning("files might not contain auditd logs")
        

    def compareRecords(self, record_a, record_b):
        """
        this function compares two event enteries and determines if one of them is obselete returns TRUE if yes FALSE otherwise 

        args: 
            record_a: first record to be compared to the second
            record_b: second record
        """
        record_a_content = regex.findall("type=(.+?).*", record_a)[-1]
        record_b_content = regex.findall("type=(.+?).*", record_b)[-1]
        record_a_time = regex.findall("time->(.+?)\n", record_a)[0]
        record_b_time = regex.findall("time->(.+?)\n",record_b)[0]
        return record_a_content == record_b_content and record_a_time == record_b_time
    
    def ifNotHave(self, reg, record): 
        """
        not all records have certain properties this function makes it so the program will input '' instead throwing an exception

        args:
            reg: the regex string to find the property
            record: the text in which the property is looked for in 
        """
        try:
            return regex.findall(reg, record)[0]
        except:
            return ""

    def parse(self):
        """
        this function parses the records aquired from the system and turns them into a list of dedicated event objs
        """
        no_dups_list = []
        isDup = 0
        for file in self.log_files_list:
            cmdstr="""ausearch -if {}/{} | tail -n 100""".format(self.log_files_path, file) #limited to 100 cuz i dont have enough RAM in my vm
            (output, pstatus, err) = callProcess(cmdstr) #get logs from the specified location (config)
            record_list = output.decode().split("----") #split records to events
            record_list.pop(0)
            for record in record_list: #iterate of records
                if no_dups_list == []:
                    no_dups_list.append(record)
                    continue
                for no_dup_record in no_dups_list: #get rid of duplicate event enteries

                    if self.compareRecords(record, no_dup_record):
                        isDup=1
                        break

                if not isDup and no_dups_list != []:
                    no_dups_list.append(record)  
                isDup = 0  
        #parsing
        events_list = []
        for record in no_dups_list:
            events_list.append(Event(      
                regex.findall("msg=audit\((.+?)\)", record)[0].split(":")[1],                                               # event id                   
                regex.findall("time->(.+?)\n", record)[0][4:9] + " " + regex.findall("time->(.+?)\n", record)[0][-4:],      # date          
                regex.findall("time->(.+?)\n", record)[0][11:19],                                                           # time                                                                      
                self.ifNotHave("success=(.+?) ", record),                                                                   # success (yes/no)                         
                regex.findall("uid=(.+?) ", record)[0],                                                                     # user id                   
                self.ifNotHave("key=\"(.+?)\"", record)))                                                                   # desc                 
 
        bl.logger.info("parsing finished")
        return events_list

    