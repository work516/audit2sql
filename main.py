#!/usr/bin/python3
from accessSQL import SQL_database
from AuditCommands import Audit
import configparser
import base_logger as bl

#constants
CONFIG_FILE_PATH = "/home/reliabits/Desktop/AuditToSQL/conf/general.conf" #didnt work with relative path for some reason 

def main():
    """
    main function runs first
    """

    #getting config
    config_file = configparser.ConfigParser()
    config_file.read(CONFIG_FILE_PATH)

    # Initialize logger with logger config file
    bl.loggerFile(config_file["logger"]["logger_path"])
    bl.loggerNameInit(config_file["logger"]["logger_name"])

    # Initialize SQL database
    db = SQL_database(config_file["sql"]["db_path"])
    db.createTables()
    db.clearDatabase()

    # Audit parsing
    au = Audit(config_file["auditd"]["logs_path"])
    event_list = au.parse()
    

    #adding list to the database
    for event in event_list:
        db.insertEvent(event)
    bl.logger.info("elements inserted into sql database")

    print(db.getEvents())

if __name__ == "__main__":
    main()