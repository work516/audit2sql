from helperFuncs import callProcess

class Event:
    """
    a class used to store event data
    """

    def __init__(self, id, date, time, success, user_id, desc):
        """
        this function assembles the event obj and finds the username based on id

        args:
            id: the event id
            date: the date in which the event was recorded
            time: The time in which the event was recorded
            success: whether the event operation was a success
            used_id: the id of the user who ran the event
            desc: the key word specified to describe the event in the audit.rules file
        """

        self.id = id
        self.date = date,
        self.time = time
        self.success = success,
        self.user_id = user_id
        cmdstr="""id -nu {}""".format(self.user_id) #gets username from user id
        (output, pstatus, err) = callProcess(cmdstr)
        self.username = output.decode()
        self.desc = desc