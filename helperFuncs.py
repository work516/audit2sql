import subprocess

def callProcess(cmdstr):
    """
    this function runs a bash command string and returns result, error message and proccess status 
    
    args:
        cmdstr: the string storing the cmd command to be ran 
    """
    # search audit trail for userid
    p = subprocess.Popen(cmdstr, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p_status = p.wait()
    return output, p_status, err
